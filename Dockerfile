# build
FROM golang:1.14.4-alpine3.12 as build

# default port of auto devops is 5000
ENV PORT 5000
EXPOSE 5000

RUN mkdir /app
ADD . /app

ENV GOPROXY https://goproxy.io
ENV GIN_MODE release

WORKDIR  /app
#RUN go mod vendor
RUN go build -mod=vendor -o app .


# release
FROM alpine:3.10
RUN mkdir /app
COPY --from=build /app/app /app/app

WORKDIR  /app
CMD ["/app/app"]